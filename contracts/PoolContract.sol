pragma solidity >=0.4.21 <0.7.0;

contract ERC20 {
    function totalSupply() public view returns (uint);
    function balanceOf(address tokenOwner) public view returns (uint balance);
    function allowance(address tokenOwner, address spender) public view returns (uint remaining);
    function transfer(address to, uint tokens) public returns (bool success);
    function approve(address spender, uint tokens) public returns (bool success);
    function transferFrom(address from, address to, uint tokens) public returns (bool success);
    event Transfer(address indexed from, address indexed to, uint tokens);
    event Approval(address indexed tokenOwner, address indexed spender, uint tokens);
}

contract Pool {
    address public owner;
    bool public open = true;
    uint public allocation;
    uint public minContribution;
    uint public maxContribution;
    uint public finalTotalContribution;
    uint private contributionsLeftToDistribute;
    
    struct Contributor {
        uint256 amount;
        bool isValue;
    }
    
    mapping (address => Contributor) contributors;
    address payable[] public contributorAccounts;

    constructor (uint _allocation, uint _minContribution, uint _maxContribution) public {
        // setting the owner of the contract
        owner = msg.sender;
        
        // adding the owner to the contributors. not sure if necessary
        Contributor storage newContributor = contributors[msg.sender];
        newContributor.amount = 0;
        newContributor.isValue = true;
        contributorAccounts.push(msg.sender) - 1;
        
        allocation = _allocation;
        minContribution = _minContribution;
        maxContribution = _maxContribution;
    }
    
    modifier onlyOwner() { // Modifier
        require(
            msg.sender == owner,
            "Only owner can call this."
        );
        _;
    }
    
    modifier onlyWhenStillOpen() { // Modifier
        require(
            open,
            "Pool is closed"
        );
        _;
    }
    
    modifier onlyWhenClosed() { // Modifier
        require(
            !open,
            "Pool is closed"
        );
        _;
    }

    function deposit() payable onlyWhenStillOpen external {
        // check whether msg.value is between min and max
        require(
            msg.value >= minContribution && msg.value <= maxContribution, 
            "Contribution too small or too large"
        );
        // check whether contribution would not exceed allocation
        // this seems to count token balances??
        require(
            address(this).balance + msg.value <= allocation,
            "Contribution would exceed allocation"
        );
        // check if address in contributors array
        if (contributors[msg.sender].isValue) {
            // add to amount of contributor in contributors array
            contributors[msg.sender].amount += msg.value;
        } else {
            //create Contributor
            Contributor storage newContributor = contributors[msg.sender];
            newContributor.amount = msg.value;
            newContributor.isValue = true;
            //push into contributors array
            contributorAccounts.push(msg.sender) - 1;
        }
    }
    
    function withdraw(uint256 requested) payable onlyWhenStillOpen external {
        //find msg.senders contribution
        uint256 available = getContributonByContributor(msg.sender);
        // contribution cannot be smaller than minContribution after withdrawal (but can be 0)
        require(
            available - requested >= minContribution || available - requested == 0,
            "Withdrawal would result in contribution being to small. You can withdraw everything, however."
        );
        //check if withdrawal request is <= contributed
        require(available >= requested, "Balance not sufficient");
            //transfer eth to msg.sender
        uint256 balanceBefore = address(this).balance;
        msg.sender.transfer(requested);
        // remove requested from contributor
        // this is not perfect yet, withdrawal by another address could interfere
        require(address(this).balance == balanceBefore - requested, "Transaction not successful");
        contributors[msg.sender].amount -= requested;
    }
    
    function toggleStatus() public onlyOwner {
        open = open ? false : true;
        if (!open) {
            finalTotalContribution = address(this).balance;
            contributionsLeftToDistribute = address(this).balance;
        }
    }
 
    function getContributors() public view returns (address payable[] memory) {
        return contributorAccounts;
    }

    function getContributonByContributor(address ins) view public returns (uint256) {
        return (contributors[ins].amount);
    }
    
    function countContributors() view public returns (uint) {
        return contributorAccounts.length;
    }
    
    function sendFunds(address payable toAddress, uint amount) payable onlyWhenClosed external onlyOwner {
        require(amount <= address(this).balance);
        toAddress.transfer(amount);
    }
    
    function claimTokens(address tokenContractAddress) payable onlyWhenClosed external {
        require(
            finalTotalContribution > 0 &&
            getContributonByContributor(msg.sender) > 0, 
            "No tokens to claim");
        // calculate share of allocation
        uint shareOfTotal = percent(getContributonByContributor(msg.sender), contributionsLeftToDistribute, 4);
        // get the balance of the contract of the token
        uint allTokens = ERC20(tokenContractAddress).balanceOf(address(this));
        require(
            allTokens > 0,
            "The contract did not receive any of the specified tokens");
        uint claimableTokens = (allTokens / 10000) * (shareOfTotal);
        ERC20(tokenContractAddress).transfer(msg.sender, claimableTokens);
        
        // deduct contribution of withdrawer from finalTotalContribution
        contributionsLeftToDistribute -= getContributonByContributor(msg.sender);
        // set contribution to 0 
        contributors[msg.sender].amount = 0;
    }
    
    function percent(uint numerator, uint denominator, uint precision) pure internal
    returns(uint quotient) {
         // caution, check safe-to-multiply here
        uint _numerator = numerator * 10 ** (precision+1);
        // with rounding of last digit
        uint _quotient = ((_numerator / denominator) + 5) / 10;
        return ( _quotient);
    }
}