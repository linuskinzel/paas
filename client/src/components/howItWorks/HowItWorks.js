import React, { Component } from "react";


class HowItWorks extends Component {

  render() {
    return (
      <div>
        <h3>How it works</h3>
        <p>1. Go to <a href="/create" target="_blank">Create Pool</a> to start.</p>
        <p>2. Choose your pool settings and deploy on the Ethereum network.</p>
        <p>3. When deployment is done, share the Pool url with potential investors.</p>
        <p>4. Close the pool at any time and invest.</p>
        <p>5. Pay out returns or re-open the Pool as required.</p>
        <br/><br/>
        <p><a href="/">Go back</a></p>
      </div>
    );
  }
}

export default HowItWorks;
