import React, { Component } from "react";
import { observer, inject } from "mobx-react";

import Button from "../core/Button";
import Input from "../core/Input";

@inject("rootStore")
@observer
class WithdrawAndContribute extends Component {
  constructor(props) {
    super(props);

    this.state = {
      contributionInputValue: "",
      withdrawalInputValue: "",
      showingContributionInput: false,
      showingWithdrawalInput: false
    };

    this.handleChange = this.handleChange.bind(this);
  }

  toggleShow(name) {
    this.setState({
      [name]: !this.state[name]
    });
  }

  handleChange(event) {
    const target = event.target;
    this.setState({
      [target.name]: target.value
    });
  }

  renderContributionInput() {
    const { existingContractStore } = this.props.rootStore;
    return (
      <div>
        <Input
          value={this.state.contributionInputValue}
          onChange={this.handleChange}
          name="contributionInputValue"
          id="input-contribution"
          placeholder="Amount in ETH"
          type="text"
          key="input-contribute"
          style={{ maxWidth: "200px" }}
        />
        <div>
          <Button
            style={{ margin: "0 auto" }}
            onClick={() =>
              existingContractStore.contribute(
                this.state.contributionInputValue
              )
            }
          >
            <p>Go</p>
          </Button>
        </div>
      </div>
    );
  }

  renderWithdrawalInput() {
    const { existingContractStore } = this.props.rootStore;
    return (
      <div>
        <Input
          value={this.state.withdrawalInputValue}
          onChange={this.handleChange}
          name="withdrawalInputValue"
          id="input-withdrawal"
          placeholder="Amount in ETH"
          type="text"
          key="input-withdrawal"
          style={{ maxWidth: "200px" }}
        />
        <div>
          <Button
            onClick={() =>
              existingContractStore.withdraw(this.state.withdrawalInputValue)
            }
            style={{ margin: "0 auto" }}
          >
            <p>Go</p>
          </Button>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="pure-g text--center mtl">
        <div className="pure-u-1 pure-u-lg-1-2 mbm">
          <Button
            onClick={() => this.toggleShow("showingContributionInput")}
            style={{ margin: "0 auto" }}
          >
            <p>Contribute</p>
          </Button>
          {this.state.showingContributionInput
            ? this.renderContributionInput()
            : null}
        </div>
        <div className="pure-u-1 pure-u-lg-1-2 mbm">
          <Button
            onClick={() => this.toggleShow("showingWithdrawalInput")}
            style={{ margin: "0 auto" }}
          >
            <p>Withdraw</p>
          </Button>

          {this.state.showingWithdrawalInput
            ? this.renderWithdrawalInput()
            : null}
        </div>
      </div>
    );
  }
}

export default WithdrawAndContribute;
