import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import { when } from "mobx";

import Button from "../core/Button";
import Input from "../core/Input";
import Navbar from '../core/Navbar'
import ContractDetails from "./ContractDetails";
import WithdrawAndContribute from "./WithdrawAndContribute";
import OwnerControls from "./OwnerControls";

@inject("rootStore")
@observer
class Contract extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showingClaimForm: false,
      tokenContractAddress: ""
    };

    when(
      () => this.props.rootStore.web3,
      () => {
        this.props.rootStore.existingContractStore.connectToPool(
          this.props.match.params.address
        );
        this.props.rootStore.getCurrentNetwork();
      }
    );

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    this.setState({
      [target.name]: target.value
    });
  }

  toggleShow(name) {
    this.setState({
      [name]: !this.state[name]
    });
  }

  render() {
    const { existingContractStore } = this.props.rootStore;
    return (
      <div>
        <Navbar />
        <div className="pure-g">
          <div className="pure-u-1-2">
            <ContractDetails />
          </div>
          <div className="pure-u-1-2">
            {existingContractStore.contractParticulars.isOpen ? (
              <WithdrawAndContribute />
            ) : null}
          </div>
        </div>
        <div>
          {existingContractStore.contractParticulars.isOpen === false ? (
            <Button
              onClick={() => {
                this.toggleShow("showingClaimForm");
              }}
            >
              <p>Claim Tokens</p>
            </Button>
          ) : null}
          {this.state.showingClaimForm ? (
            <form className="pure-form pure-form-stacked">
              <fieldset>
                <div className="pure-g">
                  <div className="pure-u-1 pure-u-md-1-3">
                    <label htmlFor="address">Token contract address</label>
                    <Input
                      className="pure-u-23-24"
                      value={this.state.tokenContractAddress}
                      onChange={this.handleChange}
                      id="tokenContractAddress"
                      name="tokenContractAddress"
                      type="text"
                    />
                  </div>

                  <div className="pure-u-1 pure-u-md-1-3">
                    <Button
                      onClick={() =>
                        existingContractStore.claimTokens(
                          this.state.tokenContractAddress
                        )
                      }
                    >
                      <p>Go</p>
                    </Button>
                  </div>
                </div>
              </fieldset>
            </form>
          ) : null}
        </div>

        {existingContractStore.isOwner ? <OwnerControls /> : null}
      </div>
    );
  }
}

export default Contract;
