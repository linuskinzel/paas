import React, { Component } from "react";
import { observer, inject } from "mobx-react";

import Button from "../core/Button";
import Input from "../core/Input";

@inject("rootStore")
@observer
class OwnerControls extends Component {
  constructor(props) {
    super(props);

    this.state = {
      transferTo: "",
      transferToAmount: "",
      showingTransferFundsForm: false
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    this.setState({
      [target.name]: target.value
    });
  }

  toggleShow(name) {
    this.setState({
      [name]: !this.state[name]
    });
  }

  render() {
    const { existingContractStore } = this.props.rootStore;
    return (
      <div>
        <h4>
          You are the owner of this Pool. Below are your extended controls.
        </h4>
        <div>
          <Button onClick={() => existingContractStore.togglePoolStatus()}>
            {existingContractStore.contractParticulars.isOpen ? (
              <p>Close Pool</p>
            ) : (
              <p>Reopen Pool</p>
            )}
          </Button>
        </div>
        <div>
          <Button
            disabled={existingContractStore.contractParticulars.isOpen}
            onClick={
              existingContractStore.contractParticulars.isOpen
                ? null
                : () => {
                    this.toggleShow("showingTransferFundsForm");
                  }
            }
          >
            <p>Transfer Funds</p>
          </Button>
          {existingContractStore.contractParticulars.isOpen ? (
            <p className="display--inline plm">Pool must be closed.</p>
          ) : null}
          {this.state.showingTransferFundsForm ? (
            <form className="pure-form pure-form-stacked">
              <fieldset>
                <div className="pure-g">
                  <div className="pure-u-1 pure-u-md-1-3">
                    <label htmlFor="address">Receiving address</label>
                    <Input
                      className="pure-u-23-24"
                      value={this.state.transferTo}
                      onChange={this.handleChange}
                      id="address"
                      name="transferTo"
                      type="text"
                    />
                  </div>

                  <div className="pure-u-1 pure-u-md-1-3">
                    <label htmlFor="amount">Amount</label>
                    <Input
                      value={this.state.transferToAmount}
                      onChange={this.handleChange}
                      id="amount"
                      name="transferToAmount"
                      type="text"
                    />
                  </div>

                  <div className="pure-u-1 pure-u-md-1-3">
                    <Button
                      onClick={() =>
                        existingContractStore.sendFunds(
                          this.state.transferTo,
                          this.state.transferToAmount
                        )
                      }
                    >
                      <p>Go</p>
                    </Button>
                  </div>
                </div>
              </fieldset>
            </form>
          ) : null}
        </div>
      </div>
    );
  }
}

export default OwnerControls;
