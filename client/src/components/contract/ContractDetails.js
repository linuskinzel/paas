import React, { Component } from "react";
import { observer, inject } from "mobx-react";

@inject("rootStore")
@observer
class ContractDetails extends Component {
  render() {
    const { existingContractStore } = this.props.rootStore;

    return (
      <div> 
        {existingContractStore.poolContractInstance ? (
          <div>            
            <h3>
              Connected to pool:{" "}
              {existingContractStore.poolContractInstance.address}
            </h3>
            <p>
              Pool Status:{" "}
              {existingContractStore.contractParticulars.isOpen === true ? (
                <span>Open</span>
              ) : existingContractStore.contractParticulars.isOpen === false ? (
                <span>Closed</span>
              ) : null}
            </p>
            <p>
              Your Contribution: {existingContractStore.contributionInEther}
            </p>
            <p>
              {existingContractStore.contractParticulars.isOpen === true ? (
                <span>
                  Current Total Contribution:{" "}
                  {existingContractStore.totalContributionInEther}
                </span>
              ) : existingContractStore.contractParticulars.isOpen === false ? (
                <span>
                  Final Total Contribution:{" "}
                  {
                    existingContractStore.contractParticulars
                      .finalTotalContribution
                  }
                </span>
              ) : null}
            </p>
            <p>
              Pool allocation: {existingContractStore.contractParticulars.alloc}
            </p>
            <p>
              Minimum Contribution:{" "}
              {existingContractStore.contractParticulars.minContribution}
            </p>
            <p>
              Maximum Contribution:{" "}
              {existingContractStore.contractParticulars.maxContribution}
            </p>
          </div>
        ) : (
          <p>Waiting for connection to Contract ...</p>
        )}
      </div>
    );
  }
}

export default ContractDetails;
