import styled from "styled-components";

const Input = styled.input`
  margin: 20px;
  padding: 5px 10px;
  text-align: center;
  cursor: pointer;
  box-shadow: none!important;
  border-radius: 0!important;
`;

export default Input