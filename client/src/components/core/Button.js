import React, { Component } from "react";
import styled from "styled-components";

class Button extends Component {
  render() {
    const ButtonNormal = styled.div`
      background: lightgray;
      margin-top: 20px;
      margin-bottom: 20px;
      padding: 5px 10px;
      max-width: 200px;
      text-align: center;
      cursor: pointer;
      min-width: 200px;
      display: inline-block;
      color: #000000;
      :hover {
        background: gray;
      }
    `;

    const ButtonDisabled = styled.div`
      background: #efefef;
      margin-top: 20px;
      margin-bottom: 20px;
      padding: 5px 10px;
      max-width: 200px;
      text-align: center;
      min-width: 200px;
      display: inline-block;
      font-style: Italic;
      color: lightgray;
    `;
    return this.props.disabled ? (
      <ButtonDisabled {...this.props}>{this.props.children}</ButtonDisabled>
    ) : (
      <ButtonNormal {...this.props}>{this.props.children}</ButtonNormal>
    );
  }
}

export default Button;
