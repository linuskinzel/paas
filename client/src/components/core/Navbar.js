import React, { Component } from "react";
import { observer, inject } from "mobx-react";

@inject("rootStore")
@observer
class Navbar extends Component {  

  render() {
    const mode = this.props.darkMode ? 'text--white' : ''
    return (
      <p className={`text--right mt0 mb0 ${mode}`}>
        Current Network: {this.props.rootStore.network}
      </p>
    );
  }
}

export default Navbar;