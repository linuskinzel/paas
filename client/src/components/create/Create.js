import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import { Link } from "react-router-dom";

import Button from "../core/Button";
import Input from "../core/Input";
import Navbar from '../core/Navbar'

@inject("rootStore")
@observer
class Create extends Component {
  constructor(props) {
    super(props);

    this.state = {
      minimumContribution: "",
      maximumContribution: "",
      alloc: ""
    };
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  // componentWillMount() {
    // if (!this.props.rootStore.web3) {
    //   this.props.rootStore.getWeb3();

    //   when(
    //     () => this.props.rootStore.web3,
    //     () => {
    //       this.props.rootStore.getCurrentNetwork();
    //     }
    //   );
    // }
  // }

  handleInputChange(event) {
    const target = event.target;
    this.setState({
      [target.name]: target.value
    });
  }

  createPool() {
    this.props.rootStore.newContractStore.createPool(
      this.state.alloc,
      this.state.minimumContribution,
      this.state.maximumContribution
    );
  }

  render() {
    const { newContractStore } = this.props.rootStore;

    return (
      <div>
        <Navbar />
        <div>
          <h3>Creating a pool</h3>          
        </div>
        {this.props.rootStore.web3 ? (
          <div>
            <form className="pure-form pure-form-aligned">
              <fieldset>
                <div className="pure-control-group">
                  <label htmlFor="minimumContribution">
                    Minimum Contribution
                  </label>
                  <Input
                    value={this.state.minimumContribution}
                    onChange={this.handleInputChange}
                    id="minimumContribution"
                    name="minimumContribution"
                    type="text"
                    key="minimumContribution"
                  />
                  <span className="pure-form-message-inline">ETH</span>
                </div>

                <div className="pure-control-group">
                  <label htmlFor="maximumContribution">
                    Maximum Contribution
                  </label>
                  <Input
                    value={this.state.maximumContribution}
                    onChange={this.handleInputChange}
                    id="maximumContribution"
                    name="maximumContribution"
                    type="text"
                    key="maximumContribution"
                  />
                  <span className="pure-form-message-inline">ETH</span>
                </div>

                <div className="pure-control-group">
                  <label htmlFor="alloc">
                    Allocation<br />(Pool Cap)
                  </label>
                  <Input
                    value={this.state.alloc}
                    onChange={this.handleInputChange}
                    id="alloc"
                    name="alloc"
                    type="text"
                    key="alloc"
                  />
                  <span className="pure-form-message-inline">ETH</span>
                </div>
              </fieldset>
            </form>
            <div
              style={{ display: "inline-block" }}
              onClick={() => this.createPool()}
            >
              <Button>
                <p>Go</p>
              </Button>
            </div>
            <Link className="no--underline" to="/">
              <p className="mlm" style={{ display: "inline-block" }}>Cancel</p>
            </Link>
          </div>
        ) : null}
        <p>{newContractStore.message}</p>
        {newContractStore.poolContractInstance ? (
          <Link
            className="no--underline"
            to={`contract/` + newContractStore.poolContractInstance.address}
          >
          <Button>
            <p>Connect now</p>
          </Button>
          </Link>
        ) : null}
      </div>
    );
  }
}

export default Create;
