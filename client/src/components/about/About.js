import React, { Component } from "react";


class About extends Component {

  render() {
    return (
      <div>
        <h3>About</h3>
        <p>Hey, <a href="http://linuskinzel.com" target="_blank">I'm Linus</a>, I wrote the Smart Contracts powering this project (Solidity) and the Frontend (React) in order to learn web3.</p>
        <p>The project allows anyone to create a trustless investment pool on Ethereum, to which other ERC-20 token holders can contribute. The Pool owner has control over the funds and can payout any returns proportionally to the contributors.</p>
        <p>To find out more or start a web3 project together, <a href="https://telegram.me/linusk1" target="_blank">hit me up on Telegram.</a></p>
        <br/><br/>
        <p><a href="/">Go back</a></p>
      </div>
    );
  }
}

export default About;
