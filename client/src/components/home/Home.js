import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import { when } from "mobx";
import { Link } from "react-router-dom";
import Particles from "react-particles-js";

import Button from "../core/Button";
import Navbar from '../core/Navbar'

@inject("rootStore")
@observer
class Home extends Component {
  constructor(props) {
    super(props);

    when(
      () => this.props.rootStore.web3,
      () => {
        this.props.rootStore.getCurrentNetwork();
      }
    );
  }

  render() {
    return (
      <div>
        <div className="home">
          <Navbar darkMode={true}/>          
          <Particles
            params={{
              particles: {
                line_linked: {
                  shadow: {
                    enable: true,
                    color: "#3CA9D1",
                    blur: 5
                  }
                }
              }
            }}
            canvasClassName="particle-canvas"
          />
          <div className="home__content">
            <div className="mtxl">
              <h1>Smart Pools</h1>
              <h3>Investment Pools powered by Smart Contracts</h3>
            </div>
            <div>
              <Link className="no--underline" to="/create">
                <Button>
                  <p>Create a pool</p>
                </Button>
              </Link>
              <p>or</p>
              <Link className="no--underline" to="/join">
                <span className="text--italic">Join a pool</span>
              </Link>
            </div>
            <div>
              <p>
                Trustless resource pooling to facilitate ICO investments and
                private ventures.
              </p>
            </div>
            <div className="display--flex width--100 mbxl">
              {/* <Link className="no--underline mrl" to="/faq">
                FAQ
              </Link> */}
              <Link className="no--underline mrl" to="/about">
                About
              </Link>
              <Link className="no--underline mrl" to="/how-it-works">
                Learn how it works
              </Link>
              {/* <Link className="no--underline mrl" to="/create">
                Smart Contract Example
              </Link> */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
