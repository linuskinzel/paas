import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import { Link } from "react-router-dom";

import Button from "../core/Button";
import Input from "../core/Input";
import Navbar from '../core/Navbar'

@inject("rootStore")
@observer
class Join extends Component {
  constructor(props) {
    super(props);

    this.state = {
      address: ""
    };
    this.handleChange = this.handleChange.bind(this);
  }

  // componentWillMount() {
  //   if (!this.props.rootStore.web3) {
  //     this.props.rootStore.getWeb3();

  //     when(
  //       () => this.props.rootStore.web3,
  //       () => {
  //         this.props.rootStore.getCurrentNetwork();
  //       }
  //     );
  //   }
  // }

  handleChange(event) {
    const target = event.target;
    this.setState({
      [target.name]: target.value
    });
  }

  render() {
    return (
      <div>
        <Navbar />
        <div>
          <h3>Joining a pool</h3>          
        </div>
        {this.props.rootStore.web3 ? (
          <div>
            <form className="pure-form pure-form-aligned">
              <fieldset>
                <div className="pure-control-group">
                  <label htmlFor="address">Address</label>
                  <Input
                    value={this.state.address}
                    onChange={this.handleChange}
                    id="input-address"
                    name="address"
                    placeholder="Address"
                    type="text"
                    key="input-address"
                  />
                </div>
              </fieldset>
            </form>
            <Link
              className="no--underline"
              to={`contract/` + this.state.address}
            >
              <Button>
                <p>Connect</p>
              </Button>
            </Link>
            <Link className="no--underline mlm" to="/">
              <p style={{ display: "inline-block" }}>Cancel</p>
            </Link>
          </div>
        ) : null}
      </div>
    );
  }
}

export default Join;
