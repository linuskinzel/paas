import React, { Component } from "react";
// import SimpleStorageContract from "./contracts/SimpleStorage.json";
import getWeb3 from "./getWeb3";
import { inject, observer } from "mobx-react";
import Crouton from "react-crouton";
import { BrowserRouter as Router, Route } from "react-router-dom";


import "./css/base.scss"

import Home from "./components/home/Home";
import Contract from "./components/contract/Contract";
import Create from "./components/create/Create";
import Join from "./components/join/Join";
import Faq from './components/faq/Faq'
import About from './components/about/About'
import HowItWorks from "./components/howItWorks/HowItWorks";

@inject("rootStore")
@observer
class App extends Component {
  // state = { storageValue: 0, web3: null, accounts: null, contract: null };

  componentDidMount = async () => {
    try {
      // Get network provider and web3 instance.
      this.props.rootStore.web3 = await getWeb3();
      this.props.rootStore.network = this.props.rootStore.getCurrentNetwork()

      // // Use web3 to get the user's accounts.
      // const accounts = await web3.eth.getAccounts();

      // // Get the contract instance.
      // const networkId = await web3.eth.net.getId();
      // const deployedNetwork = SimpleStorageContract.networks[networkId];
      // const instance = new web3.eth.Contract(
      //   SimpleStorageContract.abi,
      //   deployedNetwork && deployedNetwork.address,
      // );

      // // Set web3, accounts, and contract to the state, and then proceed with an
      // // example of interacting with the contract's methods.
      // this.setState({ web3, accounts, contract: instance }, this.runExample);
    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`,
      );
      console.error(error);
    }
  };

  render() {
    const { notificationStore } = this.props.rootStore
    return (
      <Router>
        <div className="App">
          <main className="container">
            {notificationStore.notification.id ? (
              <Crouton
                id={notificationStore.notification.id}
                message={notificationStore.notification.message}
                type={notificationStore.notification.type}
                autoMiss={notificationStore.notification.autoMiss}
              />
            ) : null}

            <Route exact path="/" component={Home} />
            <Route path="/create" component={Create} />
            <Route path="/join" component={Join} />
            <Route path="/contract/:address" component={Contract} />
            <Route path="/faq" component={Faq}/>
            <Route path="/about" component={About}/>
            <Route path="/how-it-works" component={HowItWorks} />
          </main>
        </div>
      </Router>
    );
  }
}

export default App;

  // runExample = async () => {
  //   const { accounts, contract } = this.state;

  //   // Stores a given value, 5 by default.
  //   await contract.methods.set(5).send({ from: accounts[0] });

  //   // Get the value from the contract to prove it worked.
  //   const response = await contract.methods.get().call();

  //   // Update state with the result.
  //   this.setState({ storageValue: response });
  // };

  // render() {
  //   if (!this.state.web3) {
  //     return <div>Loading Web3, accounts, and contract...</div>;
  //   }
  //   return (
  //     <div className="App">
  //       <h1>Good to Go!</h1>
  //       <p>Your Truffle Box is installed and ready.</p>
  //       <h2>Smart Contract Example</h2>
  //       <p>
  //         If your contracts compiled and migrated successfully, below will show
  //         a stored value of 5 (by default).
  //       </p>
  //       <p>
  //         Try changing the value stored on <strong>line 42</strong> of App.js.
  //       </p>
  //       <div>The stored value is: {this.state.storageValue}</div>
  //     </div>
  //   );
  // }
