import PoolContract from "../contracts/Pool.json";
import { action, observable, computed, makeObservable } from "mobx";

class ExistingContractStore {
  constructor(rootStore) {
    this.rootStore = rootStore;
    makeObservable(this);
  }

  @observable value = 0;
  @observable myContribution;
  @observable totalContribution;
  @observable
  contractParticulars = {
    minContribution: "",
    maxContribution: "",
    alloc: "",
    isOpen: null,
    finalTotalContribution: null
  };
  @observable poolContractInstance;
  @observable currentAccount = null;
  // @observable network;
  @observable isOwner = false;

  @computed
  get contributionInEther() {
    if (this.rootStore.web3) {
      if (!this.myContribution) return
      // console.log(this.myContribution)
      // console.log(typeof(this.myContribution))
      return this.rootStore.web3.utils.fromWei(this.myContribution, "ether") + " ETH";
    }
  }

  @computed
  get totalContributionInEther() {
    if (this.rootStore.web3) {
      return (
        this.rootStore.web3.utils.fromWei(this.totalContribution, "ether") + " ETH"
      );
    }
  }

  @action
  connectToPool(address) {
    const contract = require("truffle-contract");
    const poolContract = contract(PoolContract);
    poolContract.setProvider(this.rootStore.web3.currentProvider);

    this.rootStore.web3.eth.getAccounts((error, accounts) => {
      this.currentAccount = accounts[0];
      poolContract.defaults({ from: accounts[0] });
      poolContract
        .at(address)
        .then(instance => {
          this.poolContractInstance = instance;
          this.getTotalContribution();
          this.getMyContribution();
          this.getContractParticulars();
          instance.owner().then(val => {
            if (val === accounts[0]) {
              this.isOwner = true;
            }
          });
        })
        .catch(error => {
          this.existingContractMessage = error.message;
          console.log(error.message);
        });
    });
  }

  @action
  contribute(amount) {
    this.rootStore.notificationStore.notification.message =
      "Confirm in MetaMask and wait for the transaction to be mined.";
    this.poolContractInstance
      .deposit({ value: this.rootStore.web3.utils.toWei(amount, "ether") })
      .then(() => {
        this.getMyContribution();
        this.getTotalContribution();
        this.rootStore.notificationStore.notification.message = "Successfully deposited.";
      })
      .catch(error => {
        console.log(error);
        this.rootStore.notificationStore.notification.message = error.message;
      });
  }

  @action
  withdraw(amount) {
    this.rootStore.notificationStore.notification.message =
      "Confirm in MetaMask and wait for the transaction to be mined.";
    this.poolContractInstance
      .withdraw(this.rootStore.web3.utils.toWei(amount, "ether"))
      .then(() => {
        this.getMyContribution();
        this.getTotalContribution();
        this.rootStore.notificationStore.notification.message = "Successfully withdrawn.";
      })
      .catch(error => {
        this.rootStore.notificationStore.notification.message = error.message;
      });
  }

  @action
  togglePoolStatus() {
    this.rootStore.notificationStore.notification.message =
      "Confirm in MetaMask and wait for the transaction to be mined.";
    this.poolContractInstance
      .toggleStatus()
      .then(() => {
        this.rootStore.notificationStore.notification.message = "Pool status changed.";
        this.getContractParticulars();
      })
      .catch(error => {
        this.rootStore.notificationStore.notification.message = error.message;
      });
  }

  @action
  sendFunds(addressTo, amount) {
    this.rootStore.notificationStore.notification.message =
      "Confirm in MetaMask and wait for the transaction to be mined.";
    this.poolContractInstance
      .sendFunds(addressTo, this.rootStore.web3.utils.toWei(amount, "ether"))
      .then(() => {
        this.rootStore.notificationStore.notification.message = "Funds transferred.";
        this.getTotalContribution();
      })
      .catch(error => {
        this.rootStore.notificationStore.notification.message = error.message;
      });
  }

  @action
  claimTokens(tokenContractAddress) {
    this.rootStore.notificationStore.notification.message =
      "Confirm in MetaMask and wait for the transaction to be mined.";
      this.poolContractInstance
      .claimTokens(tokenContractAddress)
      .then(() => {
        this.rootStore.notificationStore.notification.message = "Funds received."
      })
      .catch(error => {
        this.rootStore.notificationStore.notification.message = error.message
      })
  }

  updateValue() {
    this.poolContractInstance.contract._eth.getBalance().then(val => {
      this.value = val.toString();
    });
  }

  getMyContribution() {
    this.poolContractInstance
      .getContributonByContributor(this.currentAccount)
      .then(val => {
        this.myContribution = val.toString();
      })
      .catch(error => {
        console.log(error);
      });
  }

  getContractParticulars() {
    this.poolContractInstance
      .open()
      .then(val => {
        this.contractParticulars.isOpen = val
      })
      .catch(error => {
        this.rootStore.notificationStore.notification.message = "Something went wrong: " + error;
      });

    this.poolContractInstance
      .allocation()
      .then(val => {
        this.contractParticulars.alloc =
          this.rootStore.web3.utils.fromWei(val, "Ether").toString() + " ETH";
      })
      .catch(error => {
        this.rootStore.notificationStore.notification.message = "Something went wrong: " + error;
      });

    this.poolContractInstance
      .minContribution()
      .then(val => {
        this.contractParticulars.minContribution =
          this.rootStore.web3.utils.fromWei(val, "Ether").toString() + " ETH";
      })
      .catch(error => {
        this.rootStore.notificationStore.notification.message = "Something went wrong: " + error;
      });

    this.poolContractInstance
      .maxContribution()
      .then(val => {
        this.contractParticulars.maxContribution =
          this.rootStore.web3.utils.fromWei(val, "Ether").toString() + " ETH";
      })
      .catch(error => {
        this.rootStore.notificationStore.notification.message = "Something went wrong: " + error;
      });

      this.poolContractInstance
      .finalTotalContribution()
      .then(val => {
        this.contractParticulars.finalTotalContribution =
          this.rootStore.web3.utils.fromWei(val, "Ether").toString() + " ETH";
      })
      .catch(error => {
        this.rootStore.notificationStore.notification.message = "Something went wrong: " + error;
      });
  }

  getTotalContribution() {
    if (this.rootStore.web3) {
      this.rootStore.web3.eth.getBalance(
        this.poolContractInstance.address,
        (err, balance) => {
          if (err) {
            this.rootStore.notificationStore.notification.message = err.message;
          } else {
            this.totalContribution = balance.toString();
          }
        }
      );
    }
  }
}

export default ExistingContractStore;
