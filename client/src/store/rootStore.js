import { action, observable, makeObservable } from "mobx";

import NewContractStore from "./newContractStore";
import ExistingContractStore from './existingContractStore'
import NotificationStore from './notificationStore'
// import getWeb3 from "../utils/getWeb3";

class RootStore {
  @observable web3 = null;
  @observable network = "Waiting for connection ... Have you installed Metamask?"

  constructor() {
    makeObservable(this);
    this.newContractStore = new NewContractStore(this);
    this.existingContractStore = new ExistingContractStore(this);
    this.notificationStore = new NotificationStore(this);
  }

  @action
  getCurrentNetwork() {
    if (this.web3) {
      this.web3.eth.net.getId((err, netId) => {
        switch (netId) {
          case 1:
            this.network = "Mainnet";
            break;
          case 2:
            this.network = "Morden (Deprecated)";
            break;
          case 3:
            this.network = "ropsten";
            break;
          case 4:
            this.network = "Rinkeby";
            break;
          case 42:
            this.network = "Kovan";
            break;
          default:
            this.network = "Unknown";
        }
      });
    }    
  }

  // @action
  // getWeb3() {
  //   getWeb3
  //     .then(results => {
  //       this.web3 = results.web3;
  //     })
  //     .catch(() => {
  //       console.log("Error finding web3.");
  //     });
  // }
}

export default RootStore;