import { observable, reaction } from "mobx";

class NotificationStore {
  @observable
  notification = {
    message: null,
    id: null,
    autoMiss: false,
    timeout: 10000,
    type:"error"
  };

  constructor(rootStore) {
    this.rootStore = rootStore;

    reaction(() => this.notification.message, () => this.show());
  }

  show() {
      this.notification.id = Date.now()
  }
}

export default NotificationStore;
