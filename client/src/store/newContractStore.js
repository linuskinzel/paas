import PoolContract from "../contracts/Pool.json";
import { action, observable, makeObservable } from "mobx";

class NewContractStore {
  constructor(rootStore) {
    makeObservable(this);
    this.rootStore = rootStore;
  }

  @observable message = null;
  @observable poolContractInstance;

  @action
  createPool(alloc, min, max) {
    const contract = require("truffle-contract");

    const web3 = this.rootStore.web3

    const allocWei = web3.utils.toWei(alloc, "Ether")
    const minWei = web3.utils.toWei(min, "Ether")
    const maxWei = web3.utils.toWei(max, "Ether")

    const poolContract = contract(PoolContract);
    poolContract.setProvider(this.rootStore.web3.currentProvider);

    this.rootStore.web3.eth.getAccounts((error, accounts) => {
      this.message = "Please confirm the transaction in Metamask and wait for the transaction to be mined."
      poolContract.defaults({ from: accounts[0] });
      poolContract
        .new(allocWei, minWei, maxWei)
        .then(instance => {
          this.poolContractInstance = instance;
          this.message =
            "Contract deployed at " + this.poolContractInstance.address;
        })
        .catch(error => {
          this.message = error.message;
          this.message += "\nHave you installed and unlocked Metamask?"
        });
    });
  }
}

export default NewContractStore;
